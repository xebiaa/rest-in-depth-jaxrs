RestInDepthMeetup jax-rs part
=================

This is the jax-rs part of the rest in depth meetup (see https://github.com/xebia/rest-in-depth-meetup).

References:
* atom/rss reader plugin for Firefox: https://addons.mozilla.org/en-US/firefox/addon/newsfox/?src=collection&collection_id=1e6169f6-6221-6f92-917f-1b3bb355f329
* Rome library: http://rometools.github.io/rome

