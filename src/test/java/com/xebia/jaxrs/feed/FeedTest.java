package com.xebia.jaxrs.feed;

import com.xebia.jaxrs.Main;
import com.xebia.jaxrs.domain.Product;
import com.xebia.jaxrs.model.ProductResource;
import com.xebia.jaxrs.repositories.ProductMemoryRepository;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Created by jan on 30/11/14.
 */
public class FeedTest {
    private HttpServer server;
    private WebTarget target;

    @Before
    public void setUp() throws Exception {
        server = Main.startServer();
        Client c = ClientBuilder.newClient();
        target = c.target(Main.BASE_URI);
        ProductMemoryRepository repository = new ProductMemoryRepository();
        Product product = new Product();
        product.setIdentifier(UUID.randomUUID());
        product.setName("Product 1");
        repository.save(product);
        product = new Product();
        product.setIdentifier(UUID.randomUUID());
        product.setName("Product 2");
        repository.save(product);
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void testFeedContainsTwoItems() {
        String responseMsg = target.path(Main.ATOM_FEED_PATH).request().get(String.class);
        assertTrue(responseMsg.indexOf("Product 1") > 0);
        assertTrue(responseMsg.indexOf("Product 2") > 0);
        assertTrue(responseMsg.indexOf("link rel=\"alternate\" href=\"http://localhost:8080/products/") > 0);
    }

    @Test
    public void testModifiedSinceReturnsOnlyNewProducts() {
        String entity = "{\"name\":\"this product should not be in result\"}";
        target.path(ProductResource.PRODUCT_URI).request().post(Entity.json(entity));
        try {
            Thread.sleep(1500);
        } catch (Exception e) {
            fail("Exception in testModifiedSinceReturnsOnlyNewProducts");
        }
        long timeStamp = System.currentTimeMillis();
        entity = "{\"name\":\"this product should be in result\"}";
        target.path(ProductResource.PRODUCT_URI).request().post(Entity.json(entity));
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");
        String date = sdf.format(timeStamp);
        System.out.println("timestamp: " + timeStamp);
        String responseMsg = target.path(Main.ATOM_FEED_PATH).request().header("If-Modified-Since",date).get(String.class);
        int count =0;
        int startAt =responseMsg.indexOf("<entry>", 0 );
        while (startAt>=0) {
            count++;
            startAt = responseMsg.indexOf("<entry>", startAt + 1);
        }
        assertEquals(1, count);
    }


}
