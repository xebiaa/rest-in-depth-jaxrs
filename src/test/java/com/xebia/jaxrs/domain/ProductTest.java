package com.xebia.jaxrs.domain;

import com.xebia.jaxrs.repositories.ProductMemoryRepository;
import org.junit.Test;

import java.util.UUID;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by jan on 04/12/14.
 */
public class ProductTest {

    @Test
    public void testUUIDIsAddedIfNotProvidedByClient() {
        Product product = new Product("my product");
        ProductMemoryRepository repo = new ProductMemoryRepository();
        repo.emptyDatabase();
        Product savedProduct = repo.save(product);
        assertNotNull(savedProduct.getIdentifier());
    }

    @Test
    public void testFindAllModifiedSinceTimestamp() {
        try {
            ProductMemoryRepository repo = new ProductMemoryRepository();
            repo.emptyDatabase();
            repo.save(new Product("My first product"));
            long modifiedTimestamp = System.currentTimeMillis();
            Thread.sleep(5);
            repo.save(new Product("My second product"));
            assertEquals(1, repo.findAllModifiedSince(modifiedTimestamp).size());
            Product modifiedProduct = repo.findAllModifiedSince(modifiedTimestamp).get(0);
            assertEquals("My second product", modifiedProduct.getName());
            assertEquals(2, repo.findAll().size());
        } catch (Exception e) {
            fail("Exception in sleep");
        }
    }

    @Test
    public void testChangingAValueUpdatesTimestamp() {
        Product product = new Product("p1", UUID.randomUUID());
        try {
            Thread.sleep(10);
            long timestamp = System.currentTimeMillis();
            Thread.sleep(10);
            product.setName("name v2");
            assertTrue(product.isModifiedSince(timestamp));
        } catch (InterruptedException e) {
            fail("Exception in sleep");
        }
    }

}
