package com.xebia.jaxrs;

import com.xebia.jaxrs.model.ProductResource;
import com.xebia.jaxrs.repositories.ProductMemoryRepository;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.client.ClientConfig;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ProductResourceTest {

    private HttpServer server;
    private WebTarget target;

    @Before
    public void setUp() throws Exception {
        server = Main.startServer();
        Client c = ClientBuilder.newClient();
        target = c.target(Main.BASE_URI);
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void testProductCanBeAddedUsingTheRESTInterface() {
        String entity = "{\"name\":\"new product\"\n" +
                ",\"identifier\":\"f3512d26-72f6-4290-9265-63ad69eccc13\"}";
        Response postResponse = target.path(ProductResource.PRODUCT_URI).request().post(Entity.json(entity));
        assertEquals(Response.Status.OK, Response.Status.fromStatusCode(postResponse.getStatus()));
        String getResponse = target.path(ProductResource.PRODUCT_URI + "f3512d26-72f6-4290-9265-63ad69eccc13").request().get(String.class);
        assertTrue(getResponse.indexOf("new product") >= 0);
    }

}

