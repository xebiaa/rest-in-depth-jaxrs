package com.xebia.jaxrs.model;

import com.xebia.jaxrs.domain.Product;
import com.xebia.jaxrs.repositories.ProductMemoryRepository;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by jan on 27/11/14.
 */

@Path(ProductResource.PRODUCT_URI)
public class ProductResource {
    public static final String PRODUCT_URI = "products/";
    private ProductMemoryRepository repository = new ProductMemoryRepository();

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public List<Product> getAllProducts() {
        return new ArrayList<>(repository.findAll());
    }

    @GET
    @Path("/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Product viewProduct(@PathParam("id") String id) {
        return repository.findById(UUID.fromString(id));
    }

    @DELETE
    @Path("/{id}")
    public void deleteProduct(@PathParam("id") String id) {
        repository.delete(UUID.fromString(id));
    }

    @POST
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Product post(Product product) {
        return repository.save(product);
    }

    @PUT
    @Path("/{id}")
    public Product updateProduct(Product product) {
        return null;
    }

}


