package com.xebia.jaxrs.domain;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.UUID;

/**
 * Created by jan on 27/11/14.
 */
@XmlRootElement
public class Product {
    private String name;
    private UUID identifier;
    private long modifiedAt;

    public Product(String name, UUID identifier) {
        this.name = name;
        this.identifier = identifier;
        updateTimestamp();
    }

    public Product(String name) {
        this.name = name;
        this.identifier = UUID.randomUUID();
        updateTimestamp();
    }

    public Product() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        updateTimestamp();
    }

    public UUID getIdentifier() {
        return identifier;
    }

    public void setIdentifier(UUID identifier) {
        this.identifier = identifier;
        updateTimestamp();
    }

    private void updateTimestamp() {
        modifiedAt = System.currentTimeMillis();
    }

    public String toString() { return "["+identifier+"," + name +"," + modifiedAt;}
    public boolean isModifiedSince(long modifiedSince) {
        return modifiedAt > modifiedSince;
    }
}
