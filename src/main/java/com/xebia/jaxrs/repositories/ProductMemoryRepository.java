package com.xebia.jaxrs.repositories;

import com.xebia.jaxrs.domain.Product;

import java.util.*;

/**
 * Created by jan on 27/11/14.
 */
public class ProductMemoryRepository{

    public synchronized Product save(Product product) {
        if (product.getIdentifier()==null) {
            product.setIdentifier(UUID.randomUUID());
        }
        this.products.put(product.getIdentifier(), product);
        return product;
    }

    public synchronized boolean delete(UUID key) {
        boolean deleted = false;
        if (products.containsKey(key)) {
            products.remove(key);
            deleted = true;
        }
        return deleted;
    }

    public Product findById(UUID key) {
        return products.get(key);
    }

    public List<Product> findAll() {
        return Collections.unmodifiableList(new ArrayList<Product>(products.values()));
    }

    public List<Product> findAllModifiedSince(long modifiedSince) {
        List<Product> result = new ArrayList<Product>();
        for (Product product : products.values()) {
            if (product.isModifiedSince(modifiedSince)) {
                result.add(product);
            }
        }
        return Collections.unmodifiableList(new ArrayList<Product>(result));
    }
    
    private static Map<UUID, Product> products = new HashMap<UUID, Product>();

    public void emptyDatabase() {
        products = new HashMap<UUID, Product>();
    }
}
