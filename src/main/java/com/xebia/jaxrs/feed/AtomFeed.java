package com.xebia.jaxrs.feed;

import com.rometools.rome.feed.synd.*;
import com.rometools.rome.io.SyndFeedOutput;
import com.xebia.jaxrs.Main;
import com.xebia.jaxrs.domain.Product;
import com.xebia.jaxrs.model.ProductResource;
import com.xebia.jaxrs.repositories.ProductMemoryRepository;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by jan on 30/11/14.
 */
@Path(Main.ATOM_FEED_PATH)
public class AtomFeed {

    private static final String FEED_TYPE = "atom_1.0";
    private ProductMemoryRepository repository = new ProductMemoryRepository();

    @GET
    @Produces({MediaType.APPLICATION_ATOM_XML})
    public String getFeed(@Context HttpHeaders httpHeaders, @Context Request request) {
        long modifiedSince = getModifiedSinceParameter(httpHeaders);
        String result = "";
        try {
            SyndFeed feed = new SyndFeedImpl();
            feed.setFeedType(FEED_TYPE);
            feed.setTitle("Sample Feed (created with ROME)");
            feed.setLink(Main.BASE_URI + Main.ATOM_FEED_PATH);
            feed.setDescription("This feed has been created using ROME");
            List entries = new ArrayList();
            for(Product product:repository.findAllModifiedSince(modifiedSince)) {
                entries.add(getProductAsSyndEntry(product));
            }
            feed.setEntries(entries);
            Writer writer = new StringWriter();
            SyndFeedOutput output = new SyndFeedOutput();
            output.output(feed, writer);
            result = writer.toString();
            writer.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("ERROR: " + ex.getMessage());
        }
        return result;
    }

    private static final DateFormat dataFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z");

    private long getModifiedSinceParameter(HttpHeaders httpHeaders) {
        List<String> modified = httpHeaders.getRequestHeader("If-Modified-Since");
        long modifiedSince = 0;
        if (modified != null && modified.size()>0) {
            String modifiedSinceString = modified.get(0);
            try {
                modifiedSince = dataFormat.parse(modifiedSinceString).getTime();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return modifiedSince;
    }

    private SyndEntry getProductAsSyndEntry(Product product) throws Exception {
        SyndEntry entry = new SyndEntryImpl();
        entry = new SyndEntryImpl();
        entry.setTitle(product.getName());
        entry.setLink(Main.BASE_URI + "products/" + product.getIdentifier());
        entry.setPublishedDate(new Date());
        SyndContent description = new SyndContentImpl();
        description.setType("text/html");
        description.setValue("<p>Product: " + product.getName() + "</p>");
        entry.setDescription(description);
        return entry;
    }
}
