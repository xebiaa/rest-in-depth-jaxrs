package com.xebia.jaxrs;

import com.xebia.jaxrs.domain.Product;
import com.xebia.jaxrs.model.ProductResource;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;
import java.util.UUID;

/**
 * Starts Grizzly HTTP server
 *
 */
public class Main {
    public static final String BASE_URI = "http://localhost:8080/";
    public static final String ATOM_FEED_PATH = "atom/";

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        final ResourceConfig rc = new ResourceConfig().packages("com.xebia.jaxrs");
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();
        System.out.println(String.format("Jersey app started available at "
                + "%s\nList of products: %sproducts\nHit enter to stop it...", BASE_URI, ProductResource.PRODUCT_URI));
        System.in.read();
        server.stop();
    }
}

